# gitlab test Servers with Vagrant

## Prerequisites

Before running this Vagrantfile, you need to install the following tools on 
your machine:

- [VirtualBox](https://www.virtualbox.org/)
- [Vagrant](https://www.vagrantup.com/)
- install vagrant plugins:
 $ vagrant plugin install vagrant-hostmanager
 $ vagrant plugin install vagrant-vbguest

## Usage

1. Clone the repository: `git clone https://gitlab.com/yu5uf/gitlab.ci.cd.git`
2. Navigate to the project directory: `cd gitlab.ci.cd`
3. Start the Vagrant virtual machine: `vagrant up` 
 - This will create the virtual machines for test servers.
4. Connect to the virtual machine:
   - To connect to the gitlab-test: `vagrant ssh gitlab-test`

## Run App Locally
- install pip `sudo apt install python3-pip`
- install the python3-venv package`sudo apt install python3.8-venv`

- clone the repo
- goto folder that source code stands and run for testing `make test`
- run for starting app `make run` (it start on port 5000)
- run app for different port `export PORT=5004` `make run ` 
- to stop use `ctrl + c` key combination
- go to host machine and enter `http://192.168.55.55:5004` on browser

## Pipeline
- pipeline file should named `.gitlab-ci.yml`